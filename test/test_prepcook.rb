require "test/unit"
require "prepcook"

class PrepcookTest < Test::Unit::TestCase
    def test_slicing_work
        assert_equal "Finished slicing", Prepcook.work('slicing', 2)
    end

    def test_any_work
        assert_equal "Finished chopping", Prepcook.work
    end
end

