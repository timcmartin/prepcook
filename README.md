# Prepcook

A very simple gem used in a ruby tutorial for a sandwich shop

## Installation

Add this line to your application's Gemfile:

    gem 'prepcook'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install prepcook

## Usage

TODO: Write usage instructions here

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
