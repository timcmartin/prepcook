# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'prepcook/version'

Gem::Specification.new do |gem|
  gem.name          = "prepcook"
  gem.version       = Prepcook::VERSION
  gem.authors       = ["Tim Martin"]
  gem.email         = ["tim.martin@gettyimages.com"]
  gem.description   = %q{'Show a brief animation on the screen'}
  gem.summary       = %q{'While the prepcook is busy, show that something is happening and display what he's done.'}
  gem.homepage      = "https://github.com/timcmartin/prepcook"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
