require "prepcook/version"
require "shell-spinner"
# Testing the rake release function

module Prepcook
    
    def self.work(cook_task = "chopping", task_length = 3, item)
        self.wait_patiently(task_length)
        print "Finished #{cook_task} #{item}\n"
    end

    def self.wait_patiently(task_length = 0)
        ShellSpinner do
            sleep task_length
        end
    end
end
